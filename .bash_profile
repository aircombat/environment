export PS1="\[\033[36m\]\u\[\033[m\]@\[\033[32m\]\h:\[\033[33;1m\]\w\[\033[m\]\$ "
export CLICOLOR=1
export LSCOLORS=ExFxBxDxCxegedabagacad

# command alias
alias ls='ls -GFh --color=auto'
alias ll='ls -al'
alias grep='grep -n --color'
