syntax on

set tabstop=4
set expandtab
set shiftwidth=4
set autoindent
set smartindent

set paste
set nu
set ruler
set hlsearch
